# Table of Contents

- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [Architecture](#architecture)
- [Implementation](#implementation)
  - [Notes](#notes)
- [Use in CI/CD Environments](#use-in-ci-cd-environments)
  - [Gitlab](#gitlab)

# Introduction

This readme is meant to serve as a guide for how one might integrate their repositories and applications with Hasura when independent projects need to reference/share the same database.

The high-level idea is that the Hasura configuration is maintained in a separate repo, but used as a submodule in each project. Then, some hooks and scripting are used to make sure the migration state does not diverge.

# Architecture

- This repo is used as a shared, central submodule between individual project repos
- Each project repo configures git hooks to keep the submodule contents up to date
- Contributors from each project can freely make alterations to the migrations/metadata, which will be pulled in automatically by hooks in other projects

_Example_:

```sh
├── hasura-multirepo-base
│   └── hasura
│       ├── metadata
│       ├── migrations
│       └── seeds
├── hasura-multirepo-project-1
│   │   --- other files ------
│   └── hasura-multirepo-base (submodule)
│       └── hasura
│           ├── metadata
│           ├── migrations
│           └── seeds
└── hasura-multirepo-project-2
    │   --- other files ------
    └── hasura-multirepo-base (submodule)
        └── hasura
            ├── metadata
            ├── migrations
            └── seeds
```

# Implementation

- Create a repo to be used as the shared submodule repo for Hasura's configuration and metadata. In this example, this is `hasura-multirepo-base`.

- Take your other repos, (`hasura-multirepo-project-1` and `hasura-multirepo-project-2` here) and add the shared repo as a git submodule

  - `git submodule add https://git(hub|lab).com/<user>/<repo> <repo>`
  - If on older version of git, run `git submodule update --init --recursive`

- Create git hooks in the repos to keep the submodule up to date automatically. Put the following in `./git/hooks/post-checkout` and `.git/hooks/post-merge`:

  - ```sh
    #!/bin/sh
    # Update all submodules from remotes recursively and merge the submodule changes, initialize if necessary
    git submodule update --init --recursive --remote --merge
    ```

- Finally, create a git hook which will ensure latest migrations are fetched, then check the migration status of the current repo before allowing a commit:

  - In `.git/hooks/pre-commit` for the dependent projects, put the following:

    - ```sh
      #!/bin/sh
      # Update all submodules from remotes recursively and merge the submodule changes, initialize if necessary
      git submodule update --init --recursive --remote --merge
      # cd to location of script
      cd ./hasura-multirepo-base/hasura || exit 1
      # Execute script to check migration status and run regression tests
      ./hasura-migrate-and-regression-tests.sh
      ```

  - `chmod` it so that it has proper permissions: `chmod +x .git/hooks/pre-commit.sh`

- And in `hasura-multirepo-base/hasura/hasura-migrate-and-regression-tests.sh` put the below:

```sh
#! /bin/bash

# scan_entries_for_not_present() parses "hasura migrate status" output like below:

# VERSION        NAME                           SOURCE STATUS  DATABASE STATUS
# 1590497881360  create_table_public_address    Present        Not Present
# 1594317688377  -                              Not Present    Present
function scan_entries_for_not_present() {
  #              timestamp        name      source status           database status
  local regex="([[:digit:]]+)\s+(\w+|-)\s+(Present|Not Present)\s+(Present|Not Present)"
  local has_missing_migrations=false

  # Iterate each line and check against the regex
  while IFS= read -r line; do
    if [[ "$line" =~ $regex ]]; then
      # Define variables from regex capture groups
      local timestamp=${BASH_REMATCH[1]}
      local name=${BASH_REMATCH[2]}
      local source_status=${BASH_REMATCH[3]}
      local database_status=${BASH_REMATCH[4]}

      if [[ $source_status = "Not Present" ]]; then
        has_missing_migrations=true
        echo "---------------------------"
        echo "Migration present in database, but not in your local source files, aborting push"
        echo "Name: $name"
        echo "Timestamp: $timestamp"
      fi

      if [[ $database_status = "Not Present" ]]; then
        has_missing_migrations=true
        echo "---------------------------"
        echo "Migration not present in database, but present in your local source files, aborting push"
        echo "Name: $name"
        echo "Timestamp: $timestamp"
      fi
    fi
  done

  if $has_missing_migrations; then
    printf "\nFOUND MISSING MIGRATIONS, ABORTING"
    exit 1
  fi
}

# Run migration checks
echo "Running check for missing migrations..."
./cli-hasura-linux-amd64 migrate status | scan_entries_for_not_present

# Run regression tests
echo "Running regressions tests for endpoint $HASURA_ENDPOINT and testsuite $HASURA_TESTSUITE_ID"

# Temporary testing env variables, can set these via the run environment
# NOTE: Maybe need admin secret here?
export HASURA_ENDPOINT=https://immortal-mullet-99.hasura.app
export HASURA_TESTSUITE_ID=dac3bdf8-3e9b-4ae7-878d-8fd5f47a4dd7

# Regression tests will throw non-zero exit code if any failures
# So we say "return successful regression test command, OR exit(1)"
./hasura-pro-linux-amd64 regression-tests run \
  --endpoint=$HASURA_ENDPOINT \
  --testsuite-id=$HASURA_TESTSUITE_ID || exit 1
```

- `chmod` it so that it has proper permissions: `chmod +x hasura-multirepo-base/hasura/hasura-migrate-and-regression-tests.sh`

## Notes

Any script can be used in place of the Python program, including probably `bash` with a combination of `awk`/`gawk` or another programming language. Python is fairly omnipresent across operating systems and so is a safe choice for compatibility purposes, but anything will do.

# Use in CI/CD Environments

The architecture above should in theory prevent any commits being made without being synced to latest changes. This makes the development process in essence identical to that of working on a single repo/project, and largely eliminates the need for any kind of advanced integration with deploy-time tooling.

To do this though, the process would be identical to above, programmatically fetching the latest metadata and then checking the migrations status.

## Gitlab

For Gitlab, the `.gitmodules` folder in the project using the shared repo must be updated to use relative paths.

(Ref: https://docs.gitlab.com/ee/ci/git_submodules.html#configuring-the-gitmodules-file)

So update this:

```toml
[submodule "hasura-multirepo-base"]
	path = hasura-multirepo-base
	url = https://gitlab.com/GavinRay97/hasura-multirepo-base.git
```

To this:

```toml
[submodule "hasura-multirepo-base"]
	path = hasura-multirepo-base
	url = ../hasura-multirepo-base.git
```

Then, you can proceed to write the Runner definition:

```yaml
# Ensure sub-modules are correctly checked-out
variables:
  GIT_SUBMODULE_STRATEGY: normal

before_script:
  - git submodule update --recursive --remote

stages:
  - Test

check_migrate_status_and_run_regression_tests:
  stage: Test
  script:
    - mkdir -p ~/.hasura
    - >
      echo 'pat: GRH28V2j1PdvKk1axzi9NjL2JpuAEltC3y36os6yzLslelfAKwjWzLidr6jO84bH' >> ~/.hasura/pro_config.yaml
    - cd ./hasura-multirepo-base/hasura || exit 1
    - ./hasura-migrate-and-regression-tests.sh
```

![](gitlab-ci-run.png)
